// main container than holds everything
import AppBar from 'material-ui/AppBar';
import React from 'react';
import UserIcon from 'material-ui/svg-icons/action/face';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import LoginIcon from 'material-ui/svg-icons/communication/vpn-key';
import SignupIcon from 'material-ui/svg-icons/image/edit';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import Divider from 'material-ui/Divider';
import { Link } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Heart from 'material-ui/svg-icons/action/favorite-border';
import CollectionIcon from 'material-ui/svg-icons/image/filter-none';

import LoginForm from '../forms/login';
import SignupForm from '../forms/signup';

class MainContainer extends React.Component {
  static defaultProps = {
    children: {}
  }
  static propTypes = {
    children: React.PropTypes.object
  }

// note: make a method, set the state & then bind it

// super prevents my other code from overriding constructor
  constructor(props) {
    super(props);
    this.handleTouchTap = this.handleTouchTap.bind(this); // binds this to event, fixes scope
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  state = {
    menuIsOpen: false,
    anchorEl: null,
    dialogIsOpen: true,
    dialogType: 'login',
    user: null
  }

// what happens on tap event
  handleTouchTap(event) {
    event.preventDefault();
    this.setState({
      menuIsOpen: true,
      anchorEl: event.target
    });
  }

  handleRequestClose() {
    this.setState({
      menuIsOpen: false
    });
  }

  handleDialogClose() {
    this.setState({
      dialogIsOpen: false
    });
  }

  handleDialogOpen(name) {
    this.setState({
      dialogType: name,
      menuIsOpen: false,
      dialogIsOpen: true
    });
  }

  handleLoginSuccess(data) {
    this.setState({
      dialogIsOpen: false,
      user: {
        name: data.email
      }
    });
  }

  render() {
    return (
      <div id="main-container">
        <AppBar
          // how to use style property in jsx
          style={{
            backgroundColor: '#94ABE0'
          }}
          title={<Link className="logo" to="/templist">Templist</Link>}
          showMenuIconButton={false}
          iconElementRight={
            // create an event on tap of button
            <FlatButton
              hoverColor="transparent"
              label={this.state.user ? this.state.user.name : null}
              labelPosition="before"
              primary
              icon={<UserIcon />}
              onTouchTap={this.handleTouchTap}
            />
          }
        />
        {/* if true, welcome user, else do nothing */}
        {/* {this.state.user ? <div>Hello, {this.state.user.name}!</div> : null} */}
        <Popover
          open={this.state.menuIsOpen} // make my popover closed by default
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
          animation={PopoverAnimationVertical}
        >
          <Menu>
            {this.state.user ? (
              <div>
                <MenuItem disabled primaryText="My Lists" leftIcon={<Heart />} onTouchTap={this.handleDialogOpen} />
                <MenuItem disabled primaryText="My Collections" leftIcon={<CollectionIcon />} onTouchTap={this.handleDialogOpen} />
                <Divider />
                <MenuItem primaryText="About" leftIcon={<InfoIcon />} containerElement={<Link to="/templist/about" />} />
                <Divider />
                <MenuItem primaryText="Logout" leftIcon={<LoginIcon />} onClick={() => window.location.reload()} />
              </div>
            ) : (
              <div>
                <MenuItem primaryText="Login" leftIcon={<LoginIcon />} onTouchTap={() => this.handleDialogOpen('login')} name="login" />
                <MenuItem primaryText="Signup" leftIcon={<SignupIcon />} onTouchTap={() => this.handleDialogOpen('signup')} name="signup" />
                <Divider />
                <MenuItem primaryText="About" leftIcon={<InfoIcon />} containerElement={<Link to="/templist/about" />} />
              </div>
            ) }
          </Menu>
        </Popover>

        <Dialog
          className="dialog"
          // title={<h1 style={{ textAlign: 'center' }}>Login to BlahBlah</h1>}
          actions={null}
          modal={false}
          open={this.state.dialogIsOpen}
          onRequestClose={this.handleDialogClose}
          contentStyle={{ maxWidth: '350px' }}
        >
          {this.state.dialogType === 'login' ? (
            <LoginForm
              onCancel={this.handleDialogClose}
              onSuccess={data => this.handleLoginSuccess(data)}
            />
          ) : (
            <SignupForm
              onCancel={this.handleDialogClose}
              onSuccess={data => this.handleLoginSuccess(data)}
            />
          )}
        </Dialog>
        {this.props.children}
      </div>
    );
  }
}

export default MainContainer;
