import injectTapEventPlugin from 'react-tap-event-plugin';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'; // adds material ui
import React from 'react';
import thunk from 'redux-thunk';

import { applyMiddleware, combineReducers, createStore } from 'redux';
import { render } from 'react-dom';
import { IndexRoute, Router, Route, browserHistory } from 'react-router';
import { routerMiddleware, routerReducer, syncHistoryWithStore } from 'react-router-redux';

import MainContainer from './containers/main.jsx';

import HomePage from './pages/home.jsx';
import AboutPage from './pages/about.jsx';
import TemplatePage from './pages/template.jsx';

injectTapEventPlugin();

const router = routerMiddleware(browserHistory);

const store = createStore(
  combineReducers({
    routing: routerReducer
  }),
  applyMiddleware(
    thunk,
    router
  )
);

// if path changes in url, it changes in app too
const history = syncHistoryWithStore(browserHistory, store);

const muiTheme = getMuiTheme({
});

render(
  <MuiThemeProvider muiTheme={muiTheme}>

    <Router history={history}>
      <Route path="/templist" component={MainContainer}>
        <IndexRoute component={HomePage} />
        <Route path="about" component={AboutPage} />
        <Route path="templates/:listType" component={TemplatePage} />
      </Route>
    </Router>


  </MuiThemeProvider>,
  document.getElementById('app')
);
