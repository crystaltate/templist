// main container than holds everything

import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import MainLogo from 'material-ui/svg-icons/content/create';

class LoginForm extends React.Component {
  static defaultProps = {
    onCancel: () => {},
    onSuccess: () => {}
  }

  static propTypes = {
    onCancel: React.PropTypes.func,
    onSuccess: React.PropTypes.func
  }

// note: make a method, set the state & then bind it

// super prevents my other code from overriding constructor
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLoginForm = this.handleLoginForm.bind(this);
  }

  state = {
    username: '',
    password: ''
  }

  handleCancel() {
    this.props.onCancel();
  }

  handleLogin() {
    firebase.auth()
      .signInWithEmailAndPassword(this.state.username, this.state.password)
      .then(function handleResponse(res) {
        this.props.onSuccess({
          email: res.email
        });
      }.bind(this))
      .catch(function handleError(error) {
        console.log(error);
      });
  }

  handleLoginForm(event) {
    const state = this.state;
    state[event.target.name] = event.target.value; // as user types, the creds will be populated
    this.setState(state);
  }

  render() {
    return (
      <div id="login-form">
        <MainLogo color="gray" style={{ width: '50px', height: '50px' }} />
        <h1>Welcome to Templist</h1>
        <h2>to-do list templates for every occasion</h2>
        <TextField autoFocus hintText="Username" label="Username" name="username" fullWidth onChange={this.handleLoginForm} />
        <TextField hintText="Password" label="Password" name="password" type="password" fullWidth onChange={this.handleLoginForm} />
        <FlatButton
          label="Cancel"
          primary // this is true by default - bool
          onTouchTap={() => this.handleCancel()}
        />
        <FlatButton
          label="Login"
          primary // this is true by default - bool
          onTouchTap={() => this.handleLogin()}
        />
      </div>
    );
  }
}

export default LoginForm;
