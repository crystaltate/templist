// main container than holds everything

import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import MainLogo from 'material-ui/svg-icons/content/create';

class SignupForm extends React.Component {
  static defaultProps = {
    onCancel: () => {},
    onSuccess: () => {}
  }

  static propTypes = {
    onCancel: React.PropTypes.func,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.handleSignup = this.handleSignup.bind(this);
    this.handleSignupForm = this.handleSignupForm.bind(this);
  }

  state = {
    username: '',
    password: ''
  }

  handleCancel() {
    this.props.onCancel();
  }

  handleSignup() {
    firebase.auth()
      .createUserWithEmailAndPassword(this.state.username, this.state.password)
      .then(function handleSignupResponse(res) {
        this.props.onSuccess({
          email: res.email
        });
      }.bind(this))
      .catch(function handleSignupError(error) {
        console.log(error);
      });
  }

  handleSignupForm(event) {
    const state = this.state;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  render() {
    return (
      <div id="signup-form">
        <MainLogo color="gray" style={{ width: '50px', height: '50px' }} />
        <h1>Signup to Templist</h1>
        <h2>to-do list templates for every occasion</h2>
        <TextField autoFocus hintText="Username" label="Username" name="username" fullWidth onChange={this.handleSignupForm} />
        <TextField hintText="Password" label="Password" name="password" type="password" fullWidth onChange={this.handleSignupForm} />
        <FlatButton
          label="Cancel"
          primary
          onTouchTap={() => this.handleCancel()}
        />
        <FlatButton
          label="Sign Up"
          primary
          onTouchTap={() => this.handleSignup()}
        />
      </div>
    );
  }
}

export default SignupForm;
