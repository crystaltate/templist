import React from 'react';
import { Card, CardActions, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Go from 'material-ui/svg-icons/content/send';

class AboutPage extends React.Component {
  render() {
    return (
      <div id="about-page">
        <p>About Templist</p>
        <Card>
          {/* <CardHeader
            title="URL Avatar"
            subtitle="Subtitle"
            avatar="images/jsa-128.jpg"
          /> */}
          <CardMedia
            overlay={<CardTitle title="About Templist" subtitle="List Templates for Any Occasion" />}
          >
            <img alt="aboutpic" src="images/notes.jpg" height="auto" width="100%" />
          </CardMedia>
          <CardTitle title="A List Template Project using ReactJS and Firebase" subtitle="by Crystal Tate" />
          <CardText>
            We create, use and discard to-do lists all the time.
            The worst part is that we constantly create lists we've created before...
            and even though we write and rewrite these lists; they don't get any better with every iteration.
            Once all the boxes are ticked, we inevitably toss our lists aside.
            Meet Templist. Here you can find a great to-do list template for that upcoming road trip.
            Make it your own (don't forget road snacks!) and save it for your next road adventure.
            Then, take it with you: Whether you use Trello, Notes, or Mail.
            You can export your ultimate checklist and take on the world.
          </CardText>
          <CardActions>
            <FlatButton label="Github Project" href="//github.com/crystal" labelPosition="after" icon={<Go />} />
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default AboutPage;
