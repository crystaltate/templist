import React from 'react';
import { Link } from 'react-router';

import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import RoadTripIcon from 'material-ui/svg-icons/maps/directions-car';
import HouseGuestIcon from 'material-ui/svg-icons/maps/hotel';
import FlightIcon from 'material-ui/svg-icons/maps/flight';
import DiningIcon from 'material-ui/svg-icons/maps/local-dining';
import MorningIcon from 'material-ui/svg-icons/image/wb-sunny';
import WeekendIcon from 'material-ui/svg-icons/hardware/videogame-asset';
import PlantIcon from 'material-ui/svg-icons/maps/local-florist';
import HouseMaintenanceIcon from 'material-ui/svg-icons/action/home';
import PetIcon from 'material-ui/svg-icons/action/pets';
import ListIcon from 'material-ui/svg-icons/av/playlist-add';

class HomePage extends React.Component {

  constructor(props) {
    super(props);
    this.getIcon = this.getIcon.bind(this);
  }

  state = {
    isLoading: true,
    templates: []
  }

  componentWillMount() {
    return firebase.database()
      .ref('/templates')
      .once('value')
      .then(function getSnapshot(snapshot) {
        const val = snapshot.val();
        const templates = [];
        for (let key in val) {
          templates.push(Object.assign(val[key], { key }));
        }
        this.setState({
          isLoading: false,
          templates
        });
      }.bind(this));
  }

  getIcon(iconName) {
    let icon;
    switch (iconName) {
      case 'road-trip': {
        icon = <RoadTripIcon />;
        break;
      }
      case 'house-guest': {
        icon = <HouseGuestIcon />;
        break;
      }
      case 'flight-trip': {
        icon = <FlightIcon />;
        break;
      }
      case 'dinner-party': {
        icon = <DiningIcon />;
        break;
      }
      case 'morning-grind': {
        icon = <MorningIcon />;
        break;
      }
      case 'weekend-checklist': {
        icon = <WeekendIcon />;
        break;
      }
      case 'houseplant-care': {
        icon = <PlantIcon />;
        break;
      }
      case 'pet-care': {
        icon = <PetIcon />;
        break;
      }
      case 'house-maintenance': {
        icon = <HouseMaintenanceIcon />;
        break;
      }
      default: {
        break;
      }
    }
    return icon;
  }

  render() {
    if (this.state.isLoading) {
      return (
        <div className="progress">
          <CircularProgress size={60} thickness={7} />
        </div>
      );
    }
    return (
      <div id="home-page">
        <h1>to-do list templates for every occasion</h1>
        <div className="collection">
          {this.state.templates.map(function (template) {
            return (
              <Link key={template.key} to={`/templist/templates/${template.key}`}>
                <Paper style={{ width: '200px', height: '200px', float: 'left', padding: '20px', margin: '10px' }} zDepth={2}>
                  {this.getIcon(template.key)}
                  <p>{template.title}</p>
                  {template.description}
                  <p><ListIcon /></p>
                </Paper>
              </Link>
            );
          }.bind(this))}
        </div>
      </div>

    );
  }
}

export default HomePage;
