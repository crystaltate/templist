import React from 'react';
import Paper from 'material-ui/Paper';
import RoadTripIcon from 'material-ui/svg-icons/maps/directions-car';
import { List, ListItem } from 'material-ui/List';
// import Subheader from 'material-ui/Subheader';
// import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import Heart from 'material-ui/svg-icons/action/favorite-border';
import CollectionIcon from 'material-ui/svg-icons/image/filter-none';
import SendIcon from 'material-ui/svg-icons/content/send';
import CircularProgress from 'material-ui/CircularProgress';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import HouseGuestIcon from 'material-ui/svg-icons/maps/hotel';
import FlightIcon from 'material-ui/svg-icons/maps/flight';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DiningIcon from 'material-ui/svg-icons/maps/local-dining';
import MorningIcon from 'material-ui/svg-icons/image/wb-sunny';
import WeekendIcon from 'material-ui/svg-icons/hardware/videogame-asset';
import PlantIcon from 'material-ui/svg-icons/maps/local-florist';
import HouseMaintenanceIcon from 'material-ui/svg-icons/action/home';
import PetIcon from 'material-ui/svg-icons/action/pets';


class TemplatePage extends React.Component {

  constructor(props) {
    super(props);
    this.handleExport = this.handleExport.bind(this);
    this.handleAuth = this.handleAuth.bind(this);
    this.handleExportDialog = this.handleExportDialog.bind(this);
    this.handleExportTitle = this.handleExportTitle.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  state = {
    template: null,
    icon: null,
    auth: null,
    exportDialogIsOpen: false,
    exportTitle: ''
  }

  // api call to firebase, to read data json file
  // page loads, compWillMount is immed. invoked, render is invoked (loader) list
  // is returned, state is updated
  componentWillMount() {
    let icon;
    const iconStyle = { width: '60px', height: '60px', padding: '0px', margin: '0 auto' };
    switch (this.props.router.params.listType) {
      case 'road-trip': {
        icon = <RoadTripIcon style={iconStyle} />;
        break;
      }
      case 'house-guest': {
        icon = <HouseGuestIcon style={iconStyle} />;
        break;
      }
      case 'flight-trip': {
        icon = <FlightIcon style={iconStyle} />;
        break;
      }
      case 'dinner-party': {
        icon = <DiningIcon style={iconStyle} />;
        break;
      }
      case 'morning-grind': {
        icon = <MorningIcon style={iconStyle} />;
        break;
      }
      case 'weekend-checklist': {
        icon = <WeekendIcon style={iconStyle} />;
        break;
      }
      case 'houseplant-care': {
        icon = <PlantIcon style={iconStyle} />;
        break;
      }
      case 'pet-care': {
        icon = <PetIcon style={iconStyle} />;
        break;
      }
      case 'house-maintenance': {
        icon = <HouseMaintenanceIcon style={iconStyle} />;
        break;
      }
      default: {
        break;
      }
    }
    this.setState({
      icon
    });
    return firebase.database()
      .ref(`/templates/${this.props.router.params.listType}`)
      .once('value')
      .then(function (snapshot) {
        this.setState({
          template: snapshot.val()
        });
      }.bind(this));
  }

  handleAuth() {
    const authenticationSuccess = function() {
      this.setState({
        auth: true
      });
      this.handleExportDialog();
    };
    const authenticationFailure = function() { console.log('Failed authentication'); };

    Trello.authorize({
      type: 'popup',
      name: 'Getting Started Application',
      scope: {
        read: 'true',
        write: 'true' },
      expiration: 'never',
      success: authenticationSuccess.bind(this),
      error: authenticationFailure
    });
  }

  handleDialogClose() {
    this.setState({
      exportDialogIsOpen: false
    });
  }

  handleExport() {
    this.setState({
      exportDialogIsOpen: false
    });

    const newBoard = {
      name: this.state.exportTitle
    };
    Trello.post('/boards/', newBoard, function (board) {
      Trello.get(`/boards/${board.id}/lists`, function (lists) {
        const myList = lists[0].id;
        this.state.template.items.forEach(function (itemText) {
          const newCard = {
            name: itemText,
            // Place this card at the bottom of my list
            idList: myList,
            pos: 'bottom'
          };
          Trello.post('/cards/', newCard);
        });
      }.bind(this));
    }.bind(this));
  }

  handleExportDialog() {
    this.setState({
      exportDialogIsOpen: true
    });
  }

  handleExportTitle(event) {
    this.setState({
      exportTitle: event.target.value

    });
  }

  render() {
    if (this.state.template === null) {
      return (
        <div className="progress">
          <CircularProgress size={60} thickness={7} />
        </div>
      );
    }
    return (
      <div id="template-page">
        <div className="list">
          <Paper style={{ width: '700px', height: 'auto', margin: '40px auto', padding: '20px', position: 'relative' }} zDepth={3}>
            <div style={{ position: 'absolute', left: '40px' }}>
              <h1>{this.state.template.title}</h1>
              <h2>{this.state.template.description}</h2>
            </div>
            <div className="actions">
              <IconButton className="disabled" tooltip="Favorite this list!">
                <Heart style={{ width: '35px', height: '35px', padding: '0px 5px' }} />
              </IconButton>
              <IconButton className="disabled" tooltip="Add to Collection">
                <CollectionIcon style={{ width: '35px', height: '35px', padding: '0px 5px' }} />
              </IconButton>
              <IconButton tooltip="Export to Trello" onTouchTap={this.state.auth ? this.handleExportDialog : this.handleAuth}>
                <SendIcon style={{ width: '35px', height: '35px', padding: '0px 5px' }} />
              </IconButton>
            </div>
            <div style={{ textAlign: 'center' }}>
              {this.state.icon}
            </div>
            <List>
              {this.state.template.items.map(function (itemText) {
                return (
                  <ListItem
                    leftCheckbox={<Checkbox style={{ top: '30px', left: '25px' }} />}
                    primaryText={<TextField fullWidth value={itemText} />}
                  />
                );
              })}
            </List>
          </Paper>
        </div>
        <Dialog
          className="dialog"
          open={this.state.exportDialogIsOpen}
          actions={[
            <FlatButton
              label="Cancel"
              primary // this is true by default - bool
              onTouchTap={this.handleDialogClose}
            />,
            <FlatButton
              label="Export to Trello"
              primary // this is true by default - bool
              onTouchTap={this.handleExport}
            />
          ]}
          modal={false}
          onRequestClose={this.handleDialogClose}
          contentStyle={{ maxWidth: '350px' }}
        >
          <h1>
            Exporting to Trello.
          </h1>
          <h2>
            Enter a board name.
          </h2>
          <TextField fullWidth onChange={this.handleExportTitle} hintText="Trip to Colorado" value={this.state.exportTitle} />
        </Dialog>
      </div>
    );
  }
}

export default TemplatePage;
