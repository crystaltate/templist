const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: [
      './app.jsx'
    ]
  },
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    contentBase: './docs',
    publicPath: '/templist/',
    hot: true,
    historyApiFallback: {
      index: 'index.html'
    },
    stats: {
      colors: true
    }
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: 'images', to: 'images' },
      { from: 'index.html' },
      { from: 'style.css' }
    ]),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.(js|jsx|es6)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: [
            'es2015',
            'react',
            'stage-0'
          ]
        }
      },
      {
        test: /\.(htm|html)$/,
        loader: 'html-loader'
      }
    ]
  },
  output: {
    publicPath: '/templist/',
    filename: 'index.js',
    path: path.join(__dirname, 'docs')
  },
  context: path.join(__dirname, 'src'),
  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.es6'
    ]
  }
};
