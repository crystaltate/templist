# Templists
#### to-do list templates for every occasion

### About Templist
We write, discard and rewrite to-do lists for the recurring events in life (air travel, road trips, hosting houseguests, etc). Even with all that practice, our to-do lists never get any better. In fact we tend to forget items each time, which actually makes them worse.

With Templist, you can modify the templates provided to make them your own. You can save them to be used again in the future. Your lists are sure to get better each time, and so will all your adventures.

### The Best Part
Export your lists to Trello for maximum task management.


With Templist, users can combine lists to make collections, and export those into the task management tools we love most. 

### Technologies Used
- Firebase (backend authentication and data storage)
- ReactJS (frontend)
- Material UI (UI Components)
- Trello API

### Future Features
- Collection Exports
- Favorites
- Export to Notes, Mail and Messages
- Tags / Grouping of Subtasks
- Text Field Edits in Templates